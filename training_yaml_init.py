from bs4 import BeautifulSoup
import requests
import argparse
import os
import sys


def irc_init_year(year, directory):
    url = 'https://dgplug.org/irclogs/{}/'.format(year)
    soup = BeautifulSoup(requests.get(url).text, 'lxml')

    links = [link.get('href') for link in soup.find_all('a') if 'Logs' in link.get('href')]

    with open(os.path.join(directory, '{}_logs.yaml'.format(year)), 'w') as outfile:
        for i, link in enumerate(links):
            outfile.write("""
- number: {}
  date: {}
  topic: ""
  logfile: {}
  speakers: ""
  content: ""
  homework: ""
""".format(str(i+1).zfill(2), link[5:15], url+link))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create an initial yaml file for dgplug logs")
    parser.add_argument("--year", help="The year that should be initialized")
    parser.add_argument("--output", help="The directory to save the output, defaults to '.'", default=".")
    args = parser.parse_args()
    if args.year is None:
        parser.print_help()
        sys.exit(1)

    irc_init_year(args.year, args.output)
